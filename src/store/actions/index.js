const increment = (nr) => {
    return {
        type: "INCREMENT",
        payload: nr
    }
}

const decrement = (nr) => {
    return {
        type: "DECREMENT",
        payload: nr,
    }
}

const signIn = () => {
    return {
        type: "SIGN_IN"
    }
}
const signOut = () => {
    return {
        type: "SIGN_OUT"
    }
}

const getObjekt = (obj) => {
    return {
        type: "GET_OBJ",
        payload: obj
    }
}

const lists = (str) =>{
    return {
        type: "LIST",
        payload: str
    }
}
const sideBar = () =>{
    return {
        type: "SIDE_BAR",
    }
}

export {
    increment,
    decrement,
    signIn,
    getObjekt,
    lists,
    sideBar,
    signOut
}
