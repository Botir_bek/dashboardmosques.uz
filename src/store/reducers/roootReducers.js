import {combineReducers} from "redux";

import counterReducer from "./counterReducer";
import loggedReducer from "./loggedReducer";
import getObjReducer from "./getObjReducer";
import listReducer from "./listReducer";
import sideBarReducer from "./sideBarReducer";

const rootReducers = combineReducers({
    counter: counterReducer,
    isLogin: loggedReducer,
    getObj: getObjReducer,
    list:listReducer,
    sideBar: sideBarReducer

})
export default rootReducers;
