export default function DoConvert(text) {
    const length = text.lotin.length;
    let str = text.lotin.split("");
    for(let i=0; i<length; i++) {
        if (str[i] === "A") {
            str[i]="А";
        }
        if (str[i] == "B") {
            str[i]="Б";
        }
        if (str[i] == "V") {
            str[i]="В";
        }
        if (str[i]+str[i+1] == "G'"||str[i]+str[i+1] == "G‘"||str[i]+str[i+1] == "G`"||str[i]+str[i+1]=="G’") {
            str[i]="Ғ";
            str.splice(i+1,1)
        }
        else if (str[i] == "G") {
            str[i]="Г";
        }
        if (str[i] == "D") {
            str[i]="Д";
        }
        if (str[i] == "E") {
            str[i]="Э";
        }
        if (str[i]+str[i+1] == "Ye"||str[i]+str[i+1] == "YE") {
            str[i]="Е";
            str.splice(i+1,1)
            
        }
        if (str[i]+str[i+1] == "Yo"||str[i]+str[i+1] == "YO") {
            str[i]="Ё";
            str.splice(i+1,1)
        }
        if (str[i] == "J") {
            str[i]="Ж";
        }
        if (str[i] == "Z") {
            str[i]="З";
        }
        if (str[i] == "I") {
            str[i]="И";
        }
        if (str[i] == "Y") {
            str[i]="Й";
        }
        if (str[i] == "K") {
            str[i]="К";
        }
        if (str[i] == "L") {
            str[i]="Л";
        }
        if (str[i] == "М") {
            str[i]="М";
        }
        if (str[i] == "N") {
            str[i]="Н";
        }
        if (str[i]+str[i+1] == "O'"||str[i]+str[i+1] == "O‘"||str[i]+str[i+1] == "O`"||str[i]+str[i+1]=="O’") {
            str[i]="Ў";
            str.splice(i+1,1)
        }else if (str[i] == "O") {
            str[i]="О";
        }
        if (str[i] == "P") {
            str[i]="П";
        }
        if (str[i] == "R") {
            str[i]="Р";
        }
        if (str[i] == "T") {
            str[i]="Т";
        }
        if (str[i] == "U") {
            str[i]="У";
        }
        if (str[i] == "F") {
            str[i]="Ф";
        }
        if (str[i] == "X") {
            str[i]="Х";
        }
        if (str[i] == "Q") {
            str[i]="Қ";
        }
        if (str[i]+str[i+1] == "Sh"||str[i]+str[i+1] == "SH") {
            str[i]="Ш";
            str.splice(i+1,1)
        }
        if (str[i]+str[i+1] == "Ch"||str[i]+str[i+1] == "CH") {
            str[i]="Ч";
            str.splice(i+1,1)
        }
        if (str[i] == "S") {
            str[i]="С";
        }
        if (str[i] == "H") {
            str[i]="Ҳ";
        }
        if (str[i]+str[i+1] == "Yu"||str[i]+str[i+1] == "YU") {
            str[i]="Ю";
            str.splice(i+1,1)
        }
        if (str[i] == "`"||str[i] == "'"||str[i]=="‘"||str[i]=="’") {
            str[i]="ь";
        }

        if (str[i]+str[i+1] == "ya") {
            str[i]="я";
            str.splice(i+1,1)
        }
        if (str[i]+str[i+1] == "yu") {
            str[i]="ю";
            str.splice(i+1,1)
        }
        if (str[i]+str[i+1] == "yo") {
            str[i] = "ё";
            str.splice(i + 1, 1)
        }
        if (str[i]+str[i+1] == "ye") {
            str[i]="е";
            str.splice(i+1,1)
        }
        if (str[i] == "e") {
            str[i]="э";
        }
        if (str[i] == "y") {
            str[i]="й";
        }

        if (str[i] == "u") {
            str[i]="у";
        }
        if (str[i] == "k") {
            str[i]="к";
        }
       
        if (str[i] == "n") {
            str[i]="н";
        }
        if (str[i]+str[i+1] == "g'"||str[i]+str[i+1] == "g‘"||str[i]+str[i+1] == "g`"||str[i]+str[i+1]=="g’") {
            str[i]="ғ";
            str.splice(i+1,1)
        }
        else if (str[i] == "g") {
            str[i]="г";
        }
        if (str[i]+str[i+1] == "sh") {
            str[i]="ш";
            str.splice(i+1,1)
        }
        if (str[i]+str[i+1] == "o'"||str[i]+str[i+1] == "o‘"||str[i]+str[i+1] == "o`"||str[i]+str[i+1]=="o’") {
            str[i]="ў";
            str.splice(i+1,1)
        }else if (str[i] == "o") {
            str[i]="о";
        }
        if (str[i] == "z") {
            str[i]="з";
        }
        if (str[i] == "x") {
            str[i]="х";
        }
        if (str[i] == "f") {
            str[i]="ф";
        }
        if (str[i] == "q") {
            str[i]="қ";
        }
        if (str[i] == "v") {
            str[i]="в";
        }
        if (str[i] == "a") {
            str[i]="а";
        }
        if (str[i] == "p") {
            str[i]="п";
        }
        if (str[i] == "r") {
            str[i]="р";
        }
        
        if (str[i] == "l") {
            str[i]="л";
        }
        if (str[i] == "d") {
            str[i]="д";
        }
        if (str[i] == "j") {
            str[i]="ж";
        }

        if (str[i] == "h") {
            str[i]="ҳ";
        }
        
        if (str[i]+str[i+1] == "ch") {
            str[i]="ч";
            str.splice(i+1,1)
        }
        if (str[i] == "s") {
            str[i]="с";
        }
        if (str[i] == "m") {
            str[i]="м";
        }
        if (str[i] == "i") {
            str[i]="и";
        }
        if (str[i] == "t") {
            str[i]="т";
        }
        if (str[i] == "b") {
            str[i]="б";
        }
      
       
        

        
        
    }
    return str.join("");

}
