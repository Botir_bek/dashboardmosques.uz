import React, { useEffect, useState } from "react";
import Users from "../Companents/Users";
import * as axios from "axios";
import Grid from "@material-ui/core/Grid";
import styled from "styled-components";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";

const Main = styled.div`
    display:flex;
    flex-wrap:wrap;
    justify-content:space-between;
`;
const Item = styled.div`
    
`;
export default function UserContainer() {
    const [user, setUser] = useState([]);

    useEffect(() => {
        getUser();
        //console.log("Load...");
    }, [])

    async function getUser() {
        const response = await axios.get("/api/mosques");
        setUser(response.data.reverse());
        console.log(user);
    }

    const searchNameOnChangeHandler = (e) => {
        //console.log("searched == ", e.target.value);
        const searchKey = e.target.value;
        let data = user;
        searchKey.length >= 0 ? setUser(data.filter(item => item.name.toLowerCase().includes(searchKey)))
            : setUser(data);

        console.log("search======>" + user.filter(item => item.name.toLowerCase().includes(searchKey)))
    };
    return (

        <Container maxWidth="lg">

            <Card style={{ padding: 20, marginTop: 20, display: "flex", justifyContent: "space-between" }}>
                <h2 >
                    Masjidlar haqida
                </h2>

                <TextField
                    id="outlined-search"
                    label="Search"
                    type="search"
                    variant="outlined"
                    onChange={searchNameOnChangeHandler}
                // InputLabelProps={{
                //     shrink: true,
                // }}
                />

            </Card>

            <Main  >


                {user.map((item, index) => (
                    <Item>
                        <Users
                            key={index}
                            user={item}
                        />
                    </Item>
                ))}

            </Main>

        </Container>




    );

}
