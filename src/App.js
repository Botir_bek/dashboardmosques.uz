import React, {Suspense } from 'react';
import "../node_modules/bootstrap/dist/css/bootstrap.css"
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom'
import AddForm from "./Companents/AddForm";
import EditForm from "./Companents/EditForm";
import ContactTable from "./Companents/ContactTable";
import LoginPage from "./Companents/LoginPage";
import Convertor from "./Companents/Convertor";
import {useSelector} from "react-redux";
import  { Layout } from './Companents'
import UserContainer from "./Container/UserContainer";





const routes = [
    { path: "/", exact: true, component: ContactTable },
    { path: "/add", exact: true, component: AddForm },
    { path: "/edit/:id", exact: true, component: EditForm },
    { path: "/convert", exact: true, component: Convertor },
    { path: "/users", exact: true, component: UserContainer },
    { path: "/auth", exact: true, component: LoginPage },

];



function App() {
    const isLogged = useSelector(state =>state.isLogin)

   let  token = localStorage.getItem('token');

   const [isLogin, setIsLogin ] =  React.useState(false)
    const sideBar = useSelector(state=> state.sideBar);

   React.useEffect(()=> {
       if (token){
           setIsLogin(true)
       }else {

           setIsLogin(false)
       }
   }, [token])

    return (
        <>
            <Router>
                <div>

                    {token ? (
                        <Layout>
                            <Suspense fallback={<div>Loading...</div>}>
                                <Switch>
                                    <div style={sideBar ? {marginLeft: 65}:{marginLeft:0}}>
                                    {routes.map((route, key) => (
                                        <Route
                                            key={key}
                                            path={route.path}
                                            component={route.component}
                                            exact
                                        />
                                    ))}
                                    </div>
                                </Switch>
                            </Suspense>
                        </Layout>
                    ) : (
                        <Suspense fallback={<div>Laoding...</div>}>
                            <Switch>
                                  <Route path="/" component={LoginPage} exact/>
                                <Redirect from="**" to="/" />
                            </Switch>
                        </Suspense>
                    )

                    }
                </div>
            </Router>
            <Router>

            </Router>
        </>
    );
}

export default App;
