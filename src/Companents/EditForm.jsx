import React, { useState, useEffect } from 'react';
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import axios from 'axios';
import { useHistory, useParams } from 'react-router-dom';
import { MenuItem } from "@material-ui/core";


export default function EditForm() {
    let history = useHistory();
    const { id } = useParams();
    console.log('id prop--->', id)
    const regins = [
        { region: "Yunusobod" },
        { region: "Chilonzor" },
        { region: "Uchtepa" },
        { region: "Bektemir" },
        { region: "Olmazor" },
        { region: "Yakkasaroy" },
        { region: "Mirzo Ulug`bek" },
        { region: "Sirg`ali" },
        { region: "Shayxontoxur" },
        { region: "Yashnobot" },
    ]
    const [user, setUser] = useState({
        name: "",
        imamName: "",
        address: "",
        region: "",
        location: "",
        busStop: "",
        miniBus: "",
        size: "",
        info: "",
        imgURL: "",
        vrURL: "",
    })

    const { name, imamName, address, region, location, busStop, miniBus, size, info, imgURL, vrURL } = user;
    const onInputChange = e => {
        console.log(e.target.value)

        setUser({ ...user, [e.target.name]: e.target.value });
    }

    useEffect(() => {
        loadUser();
        console.log(user);
    }, [])
    const onSubmit = async e => {
        e.preventDefault();
        await axios.put(`/api/mosques/${id}`, user);
        history.push("/")
    }
    const loadUser = async () => {
        const result = await axios.get(`/api/mosques/${id}`);
        setUser(result.data);
        console.log("----response-----")
        console.log(result.data);
    }

    return (
        <div>
            <Container maxWidth="lg">
                <Card style={{ padding: 20, marginTop: 10, marginBottom: 10, }} maxWidth="md">
                    <h1 style={{ textAlign: 'center' }} >Masjid ma`lumotlarini o`zgartirish</h1>

                </Card>
                <Box my={2}>
                    <Card style={{ padding: 20, marginBottom: 20, }} maxWidth="md">

                        <form onSubmit={e => onSubmit(e)}>
                            <Grid container spacing={3}>
                                <Grid item md={4}>

                                    <TextField
                                        id="filled-basic"
                                        label="Masjid nomi"
                                        style={{ width: "100%" }}
                                        variant="filled"
                                        name="name"
                                        value={user.name}
                                        onChange={e => onInputChange(e)} />


                                </Grid>
                                <Grid item md={4}>

                                    <TextField
                                        id="filled-basic"
                                        label="Imomxatib Nomi"
                                        style={{ width: "100%" }}
                                        variant="filled"
                                        name="imamName"
                                        value={user.imamName}
                                        onChange={e => onInputChange(e)} />

                                </Grid>
                                <Grid item md={4}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Manzil"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="address"
                                            value={user.address}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={4}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-select-currency"
                                            select
                                            label="Tuman"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="region"
                                            value={region}
                                            onChange={e => onInputChange(e)}>
                                            {regins.map((item, key) =>
                                                <MenuItem key={key} value={item.region}>
                                                    {item.region}
                                                </MenuItem >
                                            )}

                                        </TextField>
                                    </div>
                                </Grid>
                                <Grid item md={4}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Lokatsiya"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="location"
                                            value={user.location}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={4}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Avtobuslar"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="busStop"
                                            value={user.busStop}
                                            onChange={e => onInputChange(e)}
                                        />
                                    </div>
                                </Grid>
                                <Grid item md={6}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Mashrutkalar"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="miniBus"
                                            value={user.miniBus}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={6}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Masjid sig`imi"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="size"
                                            value={user.size}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>


                                <Grid item md={6}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Masjid rasmi linki"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="imgURL"
                                            value={user.imgURL}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={6}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Masjid VR Linki"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="vrURL"
                                            value={user.vrURL}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={12}>
                                    <div className="form-group">
                                        <TextField
                                            id="outlined-multiline-static"
                                            label="Masjid haqida"
                                            style={{ width: "100%" }}
                                            multiline
                                            rows={6}
                                            variant="outlined"
                                            name="info"
                                            value={user.info}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>

                                <Grid item md={12}>
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                    >Malumotlarni yangilash</Button>
                                </Grid>
                            </Grid>
                        </form>

                    </Card>
                </Box>
            </Container>
        </div>
    )

}
