import React from "react";
import styled from "styled-components";
import logo from '../assets/logo.png'

const FooterMain = styled.div`
    width:100%;
    background:#c0d8ed;
    margin-top: 10px;
    div{
      margin: 0 auto;
      padding-top:20px;
      text-align: center;
      
    }
  img{
      width:50px;
      height:50px;
      margin: 0 auto;
    }
`;

const FotTitle = styled.h4`
    margin: 0 auto;
    padding-top:7px;
    text-align: center;
    margin-bottom:5px;
`;
const Text = styled.p`
     margin: 0 auto;
     text-align: center;
     padding-bottom:20px;

`

export default function Footer() {
    return(
      <FooterMain>
          <div>
              <img src={logo}/>
              <FotTitle>Mosques.uz</FotTitle>
          </div>
         <Text>©Tashkent 2021 Jamoliddinov Botirbek <span style={{marginLeft:30}}></span>email: botir.bek.info@gmail.com</Text>
      </FooterMain>
    );

}
