import React, { useState } from 'react';
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import EditIcon from "@material-ui/icons/Edit";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { MenuItem } from "@material-ui/core";


export default function AddForm() {
    let history = useHistory();
    const [user, setUser] = useState({
        name: "",
        imamName: "",
        address: "",
        region: "",
        location: "",
        busStop: "",
        miniBus: "",
        size: "",
        info: "",
        imgURL: "",
        vrURL: "",
    })
    const regins = [
        { region: "Yunusobod" },
        { region: "Chilonzor" },
        { region: "Uchtepa" },
        { region: "Bektemir" },
        { region: "Olmazor" },
        { region: "Yakkasaroy" },
        { region: "Mirzo Ulug`bek" },
        { region: "Sirg`ali" },
        { region: "Shayxontoxur" },
        { region: "Yashnobot" },
    ]
    const { name, imamName, address, region, location, busStop, miniBus, size, info, imgURL, vrURL } = user;
    const onInputChange = e => {
        console.log(e.target.value)
        setUser({ ...user, [e.target.name]: e.target.value });
    }

    const onSubmit = async e => {
        e.preventDefault();
        await axios.post("/api/mosques", user);
        console.log("sssss--->u", user)
        history.push("/")
    }
    return (
        <div>
            <Container>
                <Card style={{ padding: 20, marginTop: 10, marginBottom: 10, }} maxWidth="md">
                    <h1 style={{ textAlign: 'center' }} >Masjid qo`shish</h1>

                </Card>
                <Box my={2}>
                    <Card style={{ padding: 20, marginBottom: 20, }}>
                        <form onSubmit={e => onSubmit(e)} style={{ display: "flex", justifyContent: "space-between" }}>
                            <Grid container spacing={3}>
                                <Grid item md={4}>

                                    <TextField
                                        id="filled-basic"
                                        label="Masjid nomi"
                                        style={{ width: "100%" }}
                                        variant="filled"
                                        name="name"
                                        value={name}
                                        onChange={e => onInputChange(e)} />


                                </Grid>
                                <Grid item md={4}>

                                    <TextField
                                        id="filled-basic"
                                        label="Imomxatib Nomi"
                                        style={{ width: "100%" }}
                                        variant="filled"
                                        name="imamName"
                                        value={imamName}
                                        onChange={e => onInputChange(e)} />

                                </Grid>
                                <Grid item md={4}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Manzil"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="address"
                                            value={address}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={4}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-select-currency"
                                            select
                                            label="Tuman"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="region"
                                            value={region}
                                            onChange={e => onInputChange(e)}>
                                            {regins.map((item, key) =>
                                                <MenuItem key={key} value={item.region}>
                                                    {item.region}
                                                </MenuItem >
                                            )}

                                        </TextField>
                                    </div>
                                </Grid>
                                <Grid item md={4}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Lokatsiya"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="location"
                                            value={location}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={4}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Avtobuslar"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="busStop"
                                            value={busStop}
                                            onChange={e => onInputChange(e)}
                                        />
                                    </div>
                                </Grid>
                                <Grid item md={6}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Mashrutkalar"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="miniBus"
                                            value={miniBus}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={6}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Masjid sig`imi"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="size"
                                            value={size}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>


                                <Grid item md={6}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Masjid rasmi linki"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="imgURL"
                                            value={imgURL}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={6}>
                                    <div className="form-group">
                                        <TextField
                                            id="filled-basic"
                                            label="Masjid VR Linki"
                                            style={{ width: "100%" }}
                                            variant="filled"
                                            name="vrURL"
                                            value={vrURL}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>
                                <Grid item md={12}>
                                    <div className="form-group">
                                        <TextField
                                            id="outlined-multiline-static"
                                            label="Masjid haqida"
                                            style={{ width: "100%" }}
                                            multiline
                                            rows={6}
                                            variant="outlined"
                                            name="info"
                                            value={info}
                                            onChange={e => onInputChange(e)} />
                                    </div>
                                </Grid>

                                <Grid item md={12}>
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        color="primary"
                                    >Malumotlarni qo`shish</Button>
                                </Grid>

                            </Grid>
                        </form>




                    </Card>
                </Box>
            </Container>
        </div>
    )

}
