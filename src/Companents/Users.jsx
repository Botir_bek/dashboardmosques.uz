import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ThreeDRotationIcon from '@material-ui/icons/ThreeDRotation';
import {useDispatch, useSelector} from "react-redux";
import {increment} from "../store/actions";
import RoomIcon from '@material-ui/icons/Room';

import styled from 'styled-components';

const useStyles = makeStyles((theme) => ({
    root: {
        width:400,
        marginTop: 30,
        // marginLeft: 30,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9

    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
}));
const VRBox =styled.div`
  top:0px;
  left:0px;
  width: 100%;
  height: 100vh;
  background-color: rgba(0,0,0,0.8);
  position: fixed;
  z-index: 9999999;
  display: flex;
  justify-content: center;
  iframe{
    align-self: center;
    
  }
  span{
    font-size: 40px;
    color:#fff;
    position: fixed;
    right:50px;
    top:20px;
    cursor: pointer;
  }
`;
const defURL="https://www.kmo.com.au/images/user-persona-in-web-design-and-user-expereince-.jpg";
export default function Users(props) {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [modal,setModal] = useState(false);
    const dispatch = useDispatch();
    const user = props.user;
    const handleExpandClick = () => {
        setExpanded(!expanded);

    };

    return (


        <Card className={classes.root}>
            <CardHeader
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>
                        <RoomIcon/>
                    </Avatar>
                }
                action={
                    <IconButton aria-label="settings" style={{outline:'none'}}>
                        <MoreVertIcon />
                    </IconButton>
                }
                title={user.name}
                subheader={user.location}
            />
            <CardMedia
                className={classes.media}
                image={user.imgURL}
                title={user.name}
                style={{height:100}}
            />
            <CardContent>

                <Typography variant="body2" color="textSecondary" component="p">
                        {user.message}
                    Tel: {user.busStop}

                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <IconButton
                    onClick={()=>dispatch(increment(1))}
                    aria-label="add to favorites"
                    style={{outline:'none'}}
                >
                    <FavoriteIcon />
                </IconButton>
                <IconButton
                    onClick={()=>setModal(modal=>!modal)}
                    aria-label="share"
                    style={{outline:'none'}}
                >
                    <ThreeDRotationIcon/>
                </IconButton>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                    style={{outline:'none'}}
                >
                    <ExpandMoreIcon />
                </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Typography paragraph>Masjid:</Typography>
                   <Typography>{user.info}</Typography>
                </CardContent>
            </Collapse>
            {modal &&
            <VRBox>
                <span onClick={()=>setModal(false)}>×</span>
                <iframe
                    // style={{marginLeft:60}}
                    width="90%"
                    height="80%"
                    frameBorder="0"
                    src={user.vrURL} >
                </iframe>

            </VRBox>
            }
        </Card>

    );
}
