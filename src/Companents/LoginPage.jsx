import React, {useEffect, useState, Component} from 'react';
import styled from 'styled-components';
import axios from 'axios';
import { Alert, AlertTitle } from '@material-ui/lab';
import ContactTable from "./ContactTable";
import {Link, useHistory} from "react-router-dom";
import {Dispatch} from "redux";
import {useDispatch} from "react-redux";
import {signIn} from "../store/actions";
import {
    Title,
    Bodiy,
    Main,
    Label,
    Input,
    ViewButton,
    ForgotTitle,
    LoginButton,
    Sircle1,
    Sircle2,
    Sircle3,
    Square1,
    Square2,
    Points1,
    Points2
} from "../assets/loginPageStyle"
import Snackbar from "@material-ui/core/Snackbar";
import Button from "@material-ui/core/Button";
export default  function LoginPage (){
    const dispatch = useDispatch();
    let history = useHistory();
    const  [loggedIn,setLoggedIn]=useState(false);
    const  [view,setView]=useState(false);


    const onSubmit = async  e =>{
        e.preventDefault();
        let request={
            login: document.getElementById("exampleInputName").value,
            pass: document.getElementById("exampleInputPassword").value
        }
         // await axios.post("http://localhost:3000/auth",request)
         //    .then(resp=>{
         //        {
         //            console.log(resp);
         //            dispatch(signIn());
         //
         //        }
         //    })
         //     .catch(error =>{
         //         console.log(error);
         //     })


        if((request.login=="botir"&&request.pass==1234)||(request.login=="botirbek"&&request.pass=="97arrow5")){

            dispatch(signIn());
            localStorage.setItem('token', "sd34r764g7fg7fg43");
            history.push("/");
        }else{
            setLoggedIn(true);


        }

    }
    function buttonView(){
        let value=view;
        setView(!value);
    }
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setLoggedIn(false);
    };
    return(
        <>
             <Main>
                <Sircle1></Sircle1>
                <Sircle2></Sircle2>
                <Sircle3></Sircle3>
                <Points1>
                    ...... ...... ...... ...... ...... ...... ......
                </Points1>
                <Bodiy>
                    <Title>Hello. <br/> Welcome Back</Title>
                    <div>
                        <form onSubmit={onSubmit}>
                            <div style={{display:'flex', flexDirection:'column'}}>
                                <Label >Name</Label>
                                <Input
                                    type="text"
                                    plaseholder="Name"
                                    name="login"
                                    // value={this.state.login}
                                    // onchange={this.handleChange}
                                    id="exampleInputName"/>
                            </div>
                            <div style={{display:'flex', flexDirection:'column'}}>
                                <Label >Password</Label>
                                <Input
                                    plaseholder="Passwod"
                                    name="pass"
                                    // value={this.state.pass}
                                    type={view?"text":"password"}
                                    // onchange={this.handleChange}
                                    //onChange={onHandelChange}
                                    id="exampleInputPassword"
                                    //type={view ? 'password' : 'text'}
                                />
                                <ViewButton onClick={buttonView}>VIEW</ViewButton>
                            </div>
                            <ForgotTitle >Forgot Password?</ForgotTitle>
                            <LoginButton
                                component={Link}
                                to="/"
                                type="submit"
                                className="btn btn-primary"

                            >
                                LOGIN</LoginButton>


                        </form>
                    </div>
                    <Points2>
                        ...... ...... ...... ...... ...... ...... ...... ...... ...... ......
                    </Points2>
                </Bodiy>

                <Square1></Square1>
                <Square2></Square2>
            </Main>
            <Snackbar
                open={loggedIn}
                autoHideDuration={5000}
                onClose={handleClose}
                maxSnack={3}
            >

                <Alert onClose={handleClose}  style={{background:"#f5aeae"}} severity="error">
                    Error login or Parol
                </Alert>
            </Snackbar>

        </>
    );
}
