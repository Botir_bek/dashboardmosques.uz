import React, { useEffect, useState } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from "@material-ui/core/Button";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import EditIcon from '@material-ui/icons/Edit';
import Chip from "@material-ui/core/Chip";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";
import ContactPhoneIcon from '@material-ui/icons/ContactPhone';
import EmailIcon from '@material-ui/icons/Email';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import VisibilityIcon from '@material-ui/icons/Visibility';
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import Card from "@material-ui/core/Card";
import axios from 'axios';
import Avatar from "@material-ui/core/Avatar";
import TablePagination from "@material-ui/core/TablePagination";
import DirectionsBusIcon from '@material-ui/icons/DirectionsBus';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import AirportShuttleIcon from '@material-ui/icons/AirportShuttle';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";


const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);
const useStyles = makeStyles({
    table: {
        minWidth: 700,
    },
});


export default function ContactTable(props) {
    const classes = useStyles();

    const [users, setUser] = useState([]);
    const searchNameOnChangeHandler = (e) => {
        //console.log("searched == ", e.target.value);
        const searchKey = e.target.value;
        const data = users;

        if (searchKey === "") {
            setUser(data);
            console.log("asdadadasdas");
        } else {
            setUser(users.filter(item => item.name.toLowerCase().includes(searchKey)))

        }


        // console.log("search======>"+users.filter(item=>item.name.toLowerCase().includes(searchKey)))
    };
    useEffect(() => {
        loadUser();
        console.log("Load...");
    }, [])

    const loadUser = async () => {
        const result = await axios.get("/api/mosques");
        setUser(result.data.reverse());
        console.log(result);
    };
    const onDeleteUser = async (id) => {
        if (window.confirm("Are sure to delete")) {
            await axios.delete(`/api/mosques/${id}`);
            loadUser();
        }
    }


    return (
        <Container>
            <Box my={2}>
                <Card style={{ padding: 20, marginBottom: 20, display: "flex", justifyContent: "space-between" }}>
                    <TextField
                        id="outlined-search"
                        label="Search"
                        type="search"
                        variant="outlined"
                        onChange={searchNameOnChangeHandler}
                    // InputLabelProps={{
                    //     shrink: true,
                    // }}
                    />

                    <Link to={"/add"}>
                        <IconButton style={{ outline: 'none' }} color="primary">
                            <AddCircleOutlineIcon style={{ fontSize: 30 }} />
                        </IconButton>

                    </Link>
                </Card>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell>№</StyledTableCell>
                                <StyledTableCell >Nomi</StyledTableCell>
                                <StyledTableCell align="left">Tuman</StyledTableCell>
                                <StyledTableCell align="left">Avtobus</StyledTableCell>
                                <StyledTableCell align="left">Mashrutka</StyledTableCell>
                                <StyledTableCell align="left">Kordinata</StyledTableCell>
                                <StyledTableCell align="center">Events</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {users.map((row, index) => (
                                <StyledTableRow key={row.id}>
                                    <StyledTableCell component="th" scope="row">
                                        {index + 1}
                                    </StyledTableCell>
                                    <StyledTableCell style={{ maxWidth: 250 }} component="th" scope="row">
                                        {row.name}
                                    </StyledTableCell>
                                    <StyledTableCell align="left">

                                        <Chip
                                            component={Link}
                                            to="/users"
                                            avatar={<Avatar>{row.region[0]}</Avatar>}
                                            label="Primary clickable"
                                            clickable
                                            color="primary"
                                            label={row.region} />
                                    </StyledTableCell>
                                    <StyledTableCell align="left">
                                        <DirectionsBusIcon style={{ marginRight: 5 }} />
                                        <Chip label={row.busStop} />
                                    </StyledTableCell>
                                    <StyledTableCell align="left">
                                        <AirportShuttleIcon style={{ marginRight: 5 }} />
                                        <Chip label={row.miniBus} />
                                    </StyledTableCell>
                                    <StyledTableCell align="left">
                                        <LocationOnIcon style={{ marginRight: 5 }} />
                                        <Chip variant="outlined" label={row.location} />
                                    </StyledTableCell>
                                    <StyledTableCell align="right">

                                        <Button style={{ outline: 'none' }} component={Link} to={`edit/${row.id}`} variant="outlined" color="primary" >
                                            <EditIcon />
                                        </Button>
                                        <Button style={{ outline: 'none' }} variant="outlined" color="secondary" onClick={() => onDeleteUser(row.id)} style={{ marginLeft: 10 }}>
                                            <DeleteForeverIcon />
                                        </Button>
                                    </StyledTableCell>
                                </StyledTableRow>
                            ))}
                        </TableBody>
                    </Table>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 25]}
                        component="div"
                        count={users.length}
                        rowsPerPage={1}
                        page={Math.trunc(users.length / 5)}
                    // onChangePage={handleChangePage}
                    //onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                </TableContainer>



            </Box>
        </Container>
    );
}
