import React from 'react';
import Header from "../Header";
import SideBar from "../SideBar";
import {useSelector} from "react-redux";
import Footer from "../Footer"

const Layout = ({children}) => {
const sideBar = useSelector(state=> state.sideBar);
    return (
        <div>

            <Header/>
            {sideBar && <SideBar/> }
            {/*Header*/}
            <div style={{minHeight:585}}>
            {children}
            </div>
            <Footer/>
        </div>
    );
};

export default Layout;
